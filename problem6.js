function carsonly(data,onlyList){
    var carlist = []
    for(let i = 0;i < data.length; i++){
        if(onlyList.includes(data[i]['car_make'])){
            carlist.push(data[i]);
        }
    }
    return carlist
}
module.exports = carsonly;
//console.log(carsonly(['BMW','Audi']));