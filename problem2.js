function lastCar(data){
    i = data.length - 1
    if(i >= 0){
        return 'Last car is a ' + data[i]['car_make'] + ' ' + data[i]['car_model'];
    } else {
        return 'No Car in Inventory'
    }
}
module.exports = lastCar;
//console.log(lastCar(33));