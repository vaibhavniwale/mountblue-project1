function sortedCarModel(data){
    var car_model_list = []
    for(let i = 0;i < data.length; i++){
        car_model_list.push(data[i]['car_model'])
    }
    return car_model_list.sort()
}
module.exports = sortedCarModel;
//console.log(sortedCarModel());