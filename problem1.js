function dataUsingId(data,id){
    if(data.length !== 0){
        for(let i = 0;i < data.length; i++){
            if(data[i]['id'] === id){
                return 'Car '+ data[i]['id'] + ' is a ' + data[i]['car_make'] + ' ' + data[i]['car_model'] + ' ' + data[i]['car_year'];
            }
        }
    }else{
        return []
    }
}
module.exports = dataUsingId;
//console.log(dataUsingId(33));